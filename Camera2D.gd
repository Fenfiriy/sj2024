extends Camera2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var z = zoom.x
	if Input.is_action_just_released("ZoomIn"):
		z *= 1.1
		z *= 1.1
	if Input.is_action_just_released("ZoomOut"):
		z /= 1.1
		z /= 1.1
	zoom.x = clamp(z, 0.3, 2)
	zoom.y = zoom.x
	pass

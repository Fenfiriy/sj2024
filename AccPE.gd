extends CPUParticles2D
@onready var velMin = self.initial_velocity_min
@onready var velMax = self.initial_velocity_max

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func updateVelocity(vel):
	self.initial_velocity_min = velMin + vel.project(self.transform.get_rotation())
	self.initial_velocity_max = velMax + vel
	pass

extends RigidBody2D
@export var Acceleration = 15
@export var Rotation = 3
@export var TurboMult = 2
@export var MaxSpeed = 1000
var fjp
var bjp
var accPE
var rightPE
var leftPE
var rightFPE
var leftFPE
var fuel = 10000
var s = false
@onready var colBlade = $"CollisionBlade"
var stuck:bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	var f = $"FrontJets"
	var b = $"BackJets"
	
	accPE = b.get_node("AccPE")
	rightPE = b.get_node("RightPE")
	leftPE = b.get_node("LeftPE")
	rightFPE = f.get_node("RightPE")
	leftFPE = f.get_node("LeftPE")
	
	fjp = f.transform.get_origin()
	bjp = b.transform.get_origin()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func stuff():
	fuel -= 1
	s = true

func _physics_process(_delta):
	s = false
	var rot = self.transform.get_rotation()
	var mult = 1
	
	accPE.emitting = Input.is_action_pressed("Accelerate") and fuel > 0
	rightPE.emitting = Input.is_action_pressed("Right") and fuel > 0
	leftPE.emitting = Input.is_action_pressed("Left") and fuel > 0
	rightFPE.emitting = Input.is_action_pressed("FrontRight") and fuel > 0
	leftFPE.emitting = Input.is_action_pressed("FrontLeft") and fuel > 0
	
	if fuel <= 0:
		$/root/Game/CanvasLayer/RichTextLabel.visible = true
		return
	
	if Input.is_action_pressed("Turbo"):
		mult = TurboMult
	if Input.is_action_pressed("Accelerate"):
		self.apply_force(Vector2(0, -Acceleration).rotated(rot) * mult, bjp.rotated(rot))
		stuff()
	if Input.is_action_pressed("Decelerate"):
		self.apply_force(Vector2(0, Acceleration).rotated(rot) * mult, bjp.rotated(rot))
		stuff()
	if Input.is_action_pressed("Left"):
		self.apply_force(Vector2(0, -Rotation).rotated(rot + PI/2) * mult, bjp.rotated(rot))
		stuff()
	if Input.is_action_pressed("Right"):
		self.apply_force(Vector2(0, -Rotation).rotated(rot - PI/2) * mult, bjp.rotated(rot))
		stuff()
	if Input.is_action_pressed("FrontLeft"):
		self.apply_force(Vector2(0, -Rotation).rotated(rot + PI/2) * mult, fjp.rotated(rot))
		stuff()
	if Input.is_action_pressed("FrontRight"):
		self.apply_force(Vector2(0, -Rotation).rotated(rot - PI/2) * mult, fjp.rotated(rot))
		stuff()
	if !stuck:	
		colBlade.scaleCollider(1 - min(self.linear_velocity.length(), MaxSpeed) / MaxSpeed)
		
		

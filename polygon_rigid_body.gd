extends RigidBody2D

@export var poly_threshold:float
@onready var polygon:Polygon2D = $Polygon
@onready var polygon_outline:Polygon2D = $PolygonOutline
@onready var treasure:Polygon2D = $Treasure
@onready var treasure_outline:Polygon2D = $TreasureOutline

var collision_shape
var shape_changed:bool
var digged_area1:PackedVector2Array = []
var digged_area2:PackedVector2Array = []
var gold = 0
var max_gold

func _ready():
	polygon.polygon = generate_asteroids_vertexes()
	print(polygon.polygon)
	self.contact_monitor = true
	
	# Store the polygon's global position so we can reset its position after moving its parent
	var polygon_global_position = polygon.global_position
	# Move the rigidbody to the center of the polygon, taking into account
	# any offset the Polygon2D node may have relevant to the rigidbody
	self.global_position = get_global_polygon_center(polygon)
	
	var local_polygon_center = get_local_polygon_center(polygon)
	collision_shape = CollisionPolygon2D.new()
	collision_shape.polygon = offset_local_polygon_points(local_polygon_center)
	polygon.polygon = collision_shape.polygon
	add_child(collision_shape)
	
	shape_changed = true
	
	print("new polygon center:", get_global_polygon_center(polygon))
	print("new rb position:", global_position)
	print("new polygon center local:", get_local_polygon_center(polygon))
	print("new polygon position:", polygon.global_position)
	
	polygon_outline.polygon = Geometry2D.offset_polygon(polygon.polygon, 5, 1)[0]
	
	treasure.polygon = generate_asteroids_vertexes(40, 40)
	treasure_outline.polygon = Geometry2D.offset_polygon(treasure.polygon, 2.5, 1)[0]
	
	gold = pol_area(treasure.polygon.duplicate()) * 3
	max_gold = gold

func _physics_process(delta):
	if shape_changed:
		var pol = polygon.polygon.duplicate()
		truncate_vertices(pol)
		collision_shape.polygon = pol.duplicate()
		polygon.polygon = pol.duplicate()
		self.mass = pol_area(pol) / 900
		var gold = pol_area(treasure.polygon.duplicate()) * 3
		if self.gold > gold:
			$/root/Game/Shovel.fuel += self.gold - gold
		self.gold = gold
		polygon_outline.polygon = Geometry2D.offset_polygon(polygon.polygon, 5, 1)[0]
		treasure_outline.polygon = Geometry2D.offset_polygon(treasure.polygon, 2.5, 1)[0]
		if gold/max_gold < 0.4:
			self.call_deferred("free")
	shape_changed = false

func pol_area(pol: PackedVector2Array):
	var s1 = 0
	var s2 = 0
	for i in range(-1, len(pol) - 1):
		s1 += pol[i].x * pol[i + 1].y
		s2 += pol[i + 1].x * pol[i].y
	return abs(s1 - s2) / 2 + 0.001

func truncate_vertices(pol: PackedVector2Array):
	var i = -1
	while i < len(pol) - 1:
		var v1:Vector2 = pol[i]
		var v2:Vector2 = pol[i + 1]
		if v1.distance_squared_to(v2) < poly_threshold ** 2:
			pol.remove_at(i+1)
		else:
			i += 1
	
	if len(pol) < 3:
		self.call_deferred("free")
	
	
# Offset the points of the polygon by the center of the polygon
func offset_local_polygon_points(center: Vector2):
	var adjusted_points = []
	for point in polygon.polygon:
		adjusted_points.append(self.to_local(point) - center)
	return adjusted_points

func offset_polygon_points(center: Vector2):
	var adjusted_points = []
	for point in polygon.polygon:
		adjusted_points.append(point - center)
	return adjusted_points

# A simple weighted average of all points of the polygon to find the center
func get_global_polygon_center(p):
	var center_weight = p.polygon.size()
	var center = Vector2(0, 0)
	
	for point in p.polygon:
		center.x += self.to_global(point).x / center_weight
		center.y += self.to_global(point).y / center_weight
	
	return center

func get_local_polygon_center(p):
	var center_weight = p.polygon.size()
	var center = Vector2(0, 0)
	
	for point in p.polygon:
		center.x += self.to_local(point).x / center_weight
		center.y += self.to_local(point).y / center_weight
	
	return center
	
func get_polygon_center(p):
	var center_weight = p.polygon.size()
	var center = Vector2(0, 0)
	
	for point in p.polygon:
		center.x += point.x / center_weight
		center.y += point.y / center_weight
	
	return center

func generate_asteroids_vertexes(vertex_num = 40,
 asteroid_radius = 120, variation_scale = 0.1, dispersion = 4):
	var rng = RandomNumberGenerator.new()
	var X = []
	var S = []
	var Points = []
	var mean = 0
	
	for i in range(vertex_num):
		var tmp = rng.randfn(0,dispersion)
		if tmp > 3*dispersion:
			tmp = 3*dispersion
		if tmp < -3*dispersion:
			tmp = - 3*dispersion
		X.append(tmp)
		mean += tmp
	mean = mean/vertex_num
	
	var max = 0
	var s = 0
	for i in range(vertex_num):
		X[i] = X[i]-mean
		s+=X[i]
		S.append(s)
		if abs(s) > max:
			max = abs(s)
	
	
	for i in range(vertex_num):
		S[i] = 1+S[i]/max*0.7
	#print(S)
	var S_smoothed = []
	var Smoother = [0.15, 0.2, 0.3, 0.2, 0.15]
	var pointer
	var smoothed_value
	for i in range(vertex_num):
		smoothed_value = 0
		for j in range(-2,3):
			pointer = i+j
			if pointer >= vertex_num:
				pointer = i+j-vertex_num
			smoothed_value += Smoother[j]*S[pointer]
		S_smoothed.append(smoothed_value)
	for i in range(vertex_num):
		Points.append(Vector2(0, asteroid_radius*S_smoothed[i]).rotated(2*i*PI/vertex_num))
	return PackedVector2Array(Points)
	


func _on_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	#print(body)
	#print(body.shape_owner_get_owner(body.shape_find_owner(body_shape_index)))
	var blade = body.get_node("CollisionBlade")
	if blade == body.shape_owner_get_owner(body.shape_find_owner(body_shape_index)):
		if !body.stuck:
			var pol = blade.get_node("CollisionPolygon2D")
			digged_area1.clear()
			for v in range(len(pol.polygon)):
				digged_area1.append(self.to_local(pol.to_global(pol.polygon[v])))
				
			var j = blade.get_node("Joint")
			var p = polygon.to_local(blade.to_global(blade.polygon[2]))
			if !Geometry2D.is_point_in_polygon(p, Geometry2D.offset_polygon(polygon.polygon, 3, 1)[0]):
				return
			var pt = PackedVector2Array([p, p + Vector2(0, 1), p + Vector2(1, 0)])
			dig_asteroid(polygon, Geometry2D.offset_polygon(pt, 5, 1)[0])
			dig_asteroid(treasure, Geometry2D.offset_polygon(pt, 5, 1)[0])
			j.position = blade.polygon[2]
			j.node_a = self.get_path()
			j.node_b = body.get_path()
			$dig1.polygon = digged_area1.duplicate()
			body.stuck = true
		else:
			var pol = blade.get_node("CollisionPolygon2D")
			digged_area2.clear()
			for v in range(len(pol.polygon)):
				digged_area2.append(polygon.to_local(pol.to_global(pol.polygon[v])))
			var digged_area = Geometry2D.convex_hull(Geometry2D.merge_polygons(digged_area1, digged_area2)[0])
			print(digged_area)
			dig_asteroid(polygon, Geometry2D.offset_polygon(digged_area, 3, 1)[0])
			dig_asteroid(treasure, Geometry2D.offset_polygon(digged_area, 3, 1)[0])
			var j = blade.get_node("Joint")
			j.node_a = NodePath("")
			j.node_b = NodePath("")
			
			$dig2.polygon = digged_area2.duplicate()
			body.stuck = false
			
	#print(polygon.polygon)
	#print(Geometry2D.clip_polygons(digged_area, polygon.polygon))
	#polygon.polygon = Geometry2D.clip_polygons(polygon.polygon, digged_area)[0]

	
	
func dig_asteroid(asteroid, kopata):
	shape_changed = true
	var points_kopata = Array(kopata)
	var points_asteroid = Array(asteroid.polygon)
	for i in range(len(points_kopata)):
		points_kopata[i] = asteroid.to_global(points_kopata[i])
	for i in range(len(points_asteroid)):
		points_asteroid[i] = asteroid.to_global(points_asteroid[i])
		
	# print(points_kopata)
	# print(points_asteroid)
	
	
	var newGeo = Geometry2D.clip_polygons(points_asteroid, points_kopata)
	for i in range(len(newGeo[0])):
		newGeo[0][i] = asteroid.to_local(newGeo[0][i])
	#print(len(newGeo))
	# print(newGeo[0])
	asteroid.polygon = newGeo[0]

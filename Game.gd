extends Node2D
var rng = RandomNumberGenerator.new()
@onready var screen:Camera2D = $Shovel/Camera2D
@onready var pointer:Polygon2D = $Pointer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if get_node_or_null("Asteroid") == null:
		var a = load("res://asteroid.tscn").instantiate()
		a.global_position = $Shovel.global_position + 1000 * Vector2.from_angle(rng.randf() * 2 * PI)
		add_child(a)
	
	var viewport = screen.get_viewport_rect()
	#print(viewport, screen.global_position)
	var zoom = $Shovel/Camera2D.zoom.x
	var pos = screen.global_position
	var size = viewport.size / zoom
	viewport = Rect2(pos, size)
	var shovel_pos = $Shovel.global_position #pos + size/2
	
	if get_node_or_null("Asteroid") != null:
		var gold_pos:Vector2 = $Asteroid.global_position
		var direction_to_gold:Vector2 = gold_pos - shovel_pos
		var direction_to_gold_norm = direction_to_gold / direction_to_gold.length()
		print(direction_to_gold)
		var multiplier:float
		var pointer_pos:Vector2
	
		if direction_to_gold.length() < 400 / zoom:
			pointer.visible = false
		else:
			pointer.visible = true
			pointer_pos = shovel_pos + direction_to_gold_norm * 200 / zoom
		pointer.global_position = pointer_pos

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()

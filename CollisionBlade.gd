extends CollisionPolygon2D
@onready var defPolygon = self.polygon
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func scaleCollider(_scale):
	var a = self.polygon[0]
	var b = self.polygon[-1]
	for i in range(1, self.polygon.size()):
		var t = lerp(a, b, (defPolygon[i].x - b.x)/(a.x - b.x))
		self.polygon[i].y = t.y + _scale * (defPolygon[i].y - 1 - t.y)

	var che = self.polygon
	var t = defPolygon.slice(1, -1)
	t.reverse()
	che.append_array(t)
	$CollisionPolygon2D.polygon = che
	pass
